import ballerina/http;
#remember to change int to something that cannot be overwritten!
public type StudentRecord record {
    readonly string student_num;
    string name;
    string course;
    decimal mark;
    
};

# Description
# keeps track of created students
# 
public type CreatedStudents record {
    *http:Created;
    StudentRecord[] body;
};

# Invalid Student Error, used when student nums conflict with existing ones
public type InvalidStudentNumError record {
    *http:Conflict;
    ErrorMsg body;

};

public type ErrorMsg record {
    string errmsg;
};

# Students Table 
public final table<StudentRecord> key(student_num)  StudentTable = table [
    {student_num: "220090210" , name: "Adrian", course:"Computer Science" , mark: 87}
    
];
#service function to retrieve data from students table
# use resource function to get requests
service /StudentRecord/students on new http:Listener(9000) {
    resource function get students() returns StudentRecord[]  {
        return StudentTable.toArray();
    }
    # may have to change the string to an int to accomodate it in the arrays length?
   
 resource function post countries(@http:Payload StudentRecord[] StudentRecords)
                                    returns CreatedStudents|InvalidStudentNumError {
    
    string [] conflictingStudents = from StudentRecord studentRecord in StudentRecords
        where StudentTable.hasKey(studentRecord.student_num)
        select studentRecord.student_num;

    if conflictingStudents.length() > 0 {
        return <InvalidStudentNumError>{
            body: {
                errmsg: string:'join(" ", "Student Numbers:", ...conflictingStudents)
            }
        };
    } else {
        StudentRecords.forEach(studentRecord => StudentTable.add(student_num));
        return <CreatedStudents>{body: StudentRecords};
    }
    resource function get students/[string student_num]() returns StudentRecord|InvalidStudentNumError {
    StudentRecord? studentRecord = StudentTable[student_num];
    if studentRecord is () {
        return {
            body: {
                errmsg: string `Invalid Student Number: ${student_num}`
            }
        };
    }
    return studentRecord;
}
}
